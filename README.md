# alphadev

Site consists of three repositorys. One for frontend, one for backend and one for holding a http-server that delivers the frontend code for clients.

## Install

_Make sure that you install all repositorys in the same folder_

```bash
/alphadev_fe
/alphadev_be
/alphadevwebserver
```

For FE:
_run npm install_ to install dependencies
_npm run dev_ to start project

## Testing

1. Start FE project
2. Start BE projcet
3. Run command in frontend folder _npm run cypress:open_ to open test dashboard or _npm run cypress:x_ to run test in CMD

## Deploy

Deploys are automatically triggerd on push to the master branch of _alphadev_be_ and _alphadev_feserver_. This is done with Bitbucket Pipeline. What basically happens is that after succcesfull build it pushes to the heroku master branch which reflects the hosting environment.

To deploy frontend:

1. run _npm run build_
2. cd ../alphadevwebserver
3. Merge changes to master branch in alphadev_feserver
4. Push

## Frontend

The frontend is built with Preact and consits of a App component and few stateless components. There is also a apiHelper to handle the communication with the server.

## CLI Commands

```bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification, destination (../alphadevwebserver/public/)
npm run build

# test the production build locally
npm run serve

# run tests with cypress
npm run cypress:open
```

For detailed explanation on how things work, checkout the [CLI Readme](https://github.com/developit/preact-cli/blob/master/README.md).

## Production

Frontend https://alphadevwebserver.herokuapp.com/
Backend https://alphadevbe.herokuapp.com
