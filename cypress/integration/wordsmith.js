describe("Application runs", function() {
  it("successfully loads", function() {
    cy.visit("/"); // change URL to match your dev URL
  });

  it("successfully loads Home", function() {
    cy.get("h1").should("contain", "Textreveseraren");
  });

  it("successfully loads Textfield", function() {
    cy.get("body").find(".mdc-text-field");
  });

  it("successfully loads Button", function() {
    cy.get("body").find(".mdc-button");
  });

  it("successfully loads ReverseHistory", function() {
    cy.get("h2").should("contain", "Tidigare resultat");
  });
});

describe("Input strings", function() {
  /* String 1 */

  it("write reverse string 1", function() {
    cy.get(".mdc-text-field__input")

      .type("The red fox crosses the ice, intent on none of my business.")
      .should(
        "have.value",
        "The red fox crosses the ice, intent on none of my business."
      );
  });

  it("click on button", function() {
    cy.get(".mdc-button").click();
  });

  it("wait 5 sec and see if result is correct", function() {
    cy.wait(5000);
    cy.get(".resultWrapper").should(
      "contain",
      "ehT der xof sessorc eht eci, tnetni no enon fo ym ssenisub."
    );
  });

  /* String 2 */

  it("write reverse string 2", function() {
    cy.get(".mdc-text-field__input")

      .clear()
      .type(
        "!Abc 123? !Öåä123? foo 𝌆 bar Lorem ipsum dolor sit amet, consectetur adipiscing elit. In lacus arcu, aliquet quis imperdiet quis, congue vel ante. Maecenas vehicula ligula id lacinia venenatis. Curabitur et iaculis neque."
      )
      .should(
        "have.value",
        "!Abc 123? !Öåä123? foo 𝌆 bar Lorem ipsum dolor sit amet, consectetur adipiscing elit. In lacus arcu, aliquet quis imperdiet quis, congue vel ante. Maecenas vehicula ligula id lacinia venenatis. Curabitur et iaculis neque."
      );
  });

  it("click on button", function() {
    cy.get(".mdc-button").click();
  });

  it("wait 5 sec and see if result is correct", function() {
    cy.wait(5000);
    cy.get(".resultWrapper").should(
      "contain",
      "!cbA 321? !321äåÖ? oof 𝌆 rab meroL muspi rolod tis tema, rutetcesnoc gnicsipida tile. nI sucal ucra, teuqila siuq teidrepmi siuq, eugnoc lev etna. saneceaM alucihev alugil di ainical sitanenev. rutibaruC te silucai euqen."
    );
  });
});

/* Check history */
describe("Check history", function() {
  it("History string 1 should have place two", function() {
    cy.get(".historyWrapper>ul>li")
      .eq(1)
      .should(
        "contain",
        "ehT der xof sessorc eht eci, tnetni no enon fo ym ssenisub."
      );
  });

  it("History string 2 should have place one", function() {
    cy.get(".historyWrapper>ul>li")
      .eq(0)
      .should(
        "contain",
        "!cbA 321? !321äåÖ? oof 𝌆 rab meroL muspi rolod tis tema, rutetcesnoc gnicsipida tile. nI sucal ucra, teuqila siuq teidrepmi siuq, eugnoc lev etna. saneceaM alucihev alugil di ainical sitanenev. rutibaruC te silucai euqen."
      );
  });
});

/* Let's get erros! */
describe("Validation messages", function() {
  it("write empty string", function() {
    cy.get(".mdc-text-field__input")

      .clear()
      .should("have.value", "");
  });

  it("click on button", function() {
    cy.get(".mdc-button").click();
  });

  it("wait 5 sec and see if result is correct", function() {
    cy.wait(5000);
    cy.get(".resultErrorWrapper").should(
      "contain",
      "Ingen data att göra magi med!"
    );
  });
  //---
  it("write too long string", function() {
    cy.get(".mdc-text-field__input")

      .clear()
      .type(
        "meroL muspi rolod tis tema, rutetcesnoc gnicsipida tile. nI sucal ucra, teuqila siuq teidrepmi siuq, eugnoc lev etna. saneceaM alucihev alugil di ainical sitanenev. rutibaruC te silucai euqen. cenoD muspi oel, allignirf tege tipicsus teeroal, tipicsus ue oel. euqsetnelleP tse mauq, sutcul tege tema. qwe qwe qwe"
      );
  });

  it("click on button", function() {
    cy.get(".mdc-button").click();
  });

  it("wait 5 sec and see if result is correct", function() {
    cy.wait(5000);
    cy.get(".resultErrorWrapper").should(
      "contain",
      "Texten är tyvärr för lång. Max 300 tecken"
    );
  });
});
