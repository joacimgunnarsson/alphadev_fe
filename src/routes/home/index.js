import { h, Component } from "preact";
import style from "./style";

import TextField from "preact-material-components/TextField";
import "preact-material-components/TextField/style.css";
import Button from "preact-material-components/Button";
import "preact-material-components/Button/style.css";
import "preact-material-components/Theme/style.css";
import ReverseResult from "../../components/ReverseResult";
import ReverseHistory from "../../components/ReverseHistory";

const Home = props => (
  <div class={style.home} data-test="component-home">
    <h1>Textreveseraren</h1>
    <section>
      <TextField
        textarea={true}
        label="Skriv in din text för att revesera (Max 300 tecken)"
        onKeyUp={e => {
          props.textFieldCallbackProps(e);
        }}
      />
      <Button
        raised
        ripple
        onClick={e => {
          props.buttonCallbackProps();
        }}
      >
        Revesera text
      </Button>
    </section>
    {props.reverseResultProps && (
      <ReverseResult reverseResultProps={props.reverseResultProps} />
    )}
    {props.errorMessageProps && (
      <ReverseResult red={true} reverseResultProps={props.errorMessageProps} />
    )}
    <ReverseHistory reverseHistoryProps={props.reverseHistoryProps} />
  </div>
);

export default Home;
