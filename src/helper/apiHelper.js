export function apiHelperPostReverseString(
  item,
  api,
  handleDataCallback,
  handleLoadingCallback,
  handleErrorCallback
) {
  let _this = this;
  handleLoadingCallback(true);
  fetch(api + "/reverse", {
    method: "post",
    mode: "cors",
    headers: {
      "Content-type": "application/json; charset=UTF-8"
    },
    body: JSON.stringify({ payload: item })
  })
    .then(function(response) {
      response.json().then(function(data) {
        handleLoadingCallback(false);
        if (response.status !== 200) {
          console.log(
            "Looks like there was a problem. Status Code: " + response.status
          );
          handleErrorCallback(data);
          return;
        }

        // Examine the text in the response

        handleDataCallback(data);
      });
    })
    .catch(function(err) {
      console.log("Fetch Error :-S", err);
    });
}

export function apiHelperGetHistory(
  api,
  handleDataCallback,
  handleLoadingCallback,
  handleErrorCallback
) {
  handleLoadingCallback(true);
  fetch(api + "/history", {
    mode: "cors"
  })
    .then(function(response) {
      response.json().then(function(data) {
        handleLoadingCallback(false);
        if (response.status !== 200) {
          console.log(
            "Looks like there was a problem. Status Code: " + response.status
          );
          handleErrorCallback(data);
          return;
        }

        // Examine the text in the response
        handleDataCallback(data);
      });
    })
    .catch(function(err) {
      console.log(response);
      console.log("Fetch Error :-S", err);
    });
}
