import { h } from "preact";
import style from "./style";

const ReverseResult = props => (
  <section
    class={
      props.red
        ? style.reverseResultRed +
          " " +
          style.reverseResult +
          " resultErrorWrapper"
        : style.reverseResult + " resultWrapper"
    }
  >
    {props.reverseResultProps}
  </section>
);

export default ReverseResult;
