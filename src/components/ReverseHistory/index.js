import { h } from "preact";
import style from "./style";

let getHistoryItems = loopitems => {
  let items = loopitems.map(item => {
    return <li key={item.Id}>{item.text}</li>;
  });

  return items;
};

const ReverseHistory = props => (
  <section class={style.reverseHistory + " historyWrapper"}>
    {props.ReverseHistoryProps}
    <h2>Tidigare resultat</h2>
    <ul>{getHistoryItems(props.reverseHistoryProps)}</ul>
  </section>
);

export default ReverseHistory;
