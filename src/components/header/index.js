import { h } from "preact";
import style from "./style";

const Header = () => (
  <header class={style.header}>
    <h1>Wordsmith Inc</h1>
  </header>
);

export default Header;
