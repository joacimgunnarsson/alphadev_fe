import { h, Component } from "preact";
import { Router } from "preact-router";

// Code-splitting is automated for routes
import Home from "../routes/home";
import Header from "./header";

import LinearProgress from "preact-material-components/LinearProgress";
import "preact-material-components/LinearProgress/style.css";

import {
  apiHelperGetHistory,
  apiHelperPostReverseString
} from "../helper/apiHelper";

export default class App extends Component {
  constructor() {
    super();
    this.state.result = "";
    this.state.errorMessage = "";
    this.state.resultHistory = [];
    this.state.userInput = "";
    this.state.API =
      process.env.NODE_ENV === "production"
        ? "https://alphadevbe.herokuapp.com"
        : "http://localhost:3000";
    this.state.isLoading = false;
  }

  componentDidMount() {
    apiHelperGetHistory(
      this.state.API,
      this.handleData,
      this.handleLoading,
      this.handleErrorMessage
    );
  }

  handleRoute = e => {
    this.currentUrl = e.url;
  };

  textFieldCallback = e => {
    this.setState({ userInput: e.target.value });
  };

  buttonCallback = () => {
    apiHelperPostReverseString(
      this.state.userInput,
      this.state.API,
      this.handleData,
      this.handleLoading,
      this.handleErrorMessage
    );
  };

  handleLoading = value => {
    this.setState({ isLoading: value });
  };

  handleData = data => {
    this.setState({ result: data.result });
    this.setState({ resultHistory: data.history });
    this.setState({ errorMessage: "" });
  };

  handleErrorMessage = data => {
    this.setState({ result: "" });
    this.setState({ errorMessage: data.error });
  };

  render() {
    return (
      <div id="app">
        <Header />
        {this.state.isLoading && <LinearProgress indeterminate />}
        <Router onChange={this.handleRoute}>
          <Home
            textFieldCallbackProps={this.textFieldCallback}
            buttonCallbackProps={this.buttonCallback}
            reverseResultProps={this.state.result}
            reverseHistoryProps={this.state.resultHistory}
            errorMessageProps={this.state.errorMessage}
            path="/"
          />
        </Router>
      </div>
    );
  }
}
