import Home from "../src/routes/home";
// See: https://github.com/mzgoddard/preact-render-spy
import { shallow, deep } from "preact-render-spy";
import { TextField } from "preact-material-components/TextField";
import { Button } from "preact-material-components/Button";

describe("Initial Test of the Home", () => {
  test("Header renders 1 Textfield and 1 Button", () => {
    const context = shallow(<Home />);
    expect(context.find(".mdc-text-field")).toBe(1);
    //expect(context.find(<Button />).length).toBe(1);
  });
});
